import ExampleSimple from './ExampleSimple';
import ExampleUseClose from './ExampleUseClose';
import ExampleUseCloseBtnInContent from './ExampleUseCloseBtnInContent';
import ExampleUseCloseOutsideContent from './ExampleUseCloseOutsideContent';
import ExampleUseAnimation from './ExampleUseAnimation';
import ExampleUseAnimationInContent from './ExampleUseAnimationInContent';
import 'animate.css/source/sliding_entrances/slideInUp.css';
import 'animate.css/source/sliding_exits/slideOutDown.css';
import 'animate.css/source/fading_entrances/fadeIn.css';
import 'animate.css/source/fading_exits/fadeOut.css';
import './example.css';

const Examples = () => (
	<div id="examples">
		<h2>Examples</h2>
		<ExampleSimple />
		<ExampleUseClose />
		<ExampleUseCloseOutsideContent />
		<ExampleUseCloseBtnInContent />
		<ExampleUseAnimation />
		<ExampleUseAnimationInContent />
	</div>
);

export default Examples;
