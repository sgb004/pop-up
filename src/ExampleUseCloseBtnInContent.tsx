import PopUp from '../lib/PopUp';
import ButtonFor from '@wudev/button-for';

const ExampleUseCloseBtnInContent = () => (
	<div className="example">
		<ButtonFor htmlFor="pop-use-close-button-in-content">Use close button in content</ButtonFor>
		<PopUp id="pop-use-close-button-in-content" className="use-close-btn-in-content">
			<h3>Use close button in content</h3>
			<p>
				It is possible to put the close button into the content with the class{' '}
				<code>use-close-btn-in-content</code>.
			</p>
			<pre>
				<code>
					import PopUp from '@wudev/pop-up';
					<br />
					import ButtonFor from '@wudev/button-for';
					<br />
					<br />
					&lt;ButtonFor htmlFor="pop-up"&gt;Open PopUp&lt;/ButtonFor&gt;
					<br />
					&lt;PopUp id="pop-up" <strong>className="use-close-btn-in-content"</strong>
					&gt;...&lt;/PopUp&gt;
				</code>
			</pre>
		</PopUp>
	</div>
);

export default ExampleUseCloseBtnInContent;
