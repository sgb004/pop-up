import PopUp from '../lib/PopUp';
import ButtonFor from '@wudev/button-for';

const ExampleUseCloseInPopUp = () => (
	<div className="example">
		<ButtonFor htmlFor="pop-up-use-close-btn">Use close button</ButtonFor>
		<PopUp id="pop-up-use-close-btn" useCloseBtn={false}>
			<h3>Use close button</h3>
			<p>
				By default the close button is enabled, with the <code>useCloseBtn</code> attribute,
				it is possible disable the close button.
			</p>
			<pre>
				<code>
					import PopUp from '@wudev/pop-up';
					<br />
					import ButtonFor from '@wudev/button-for';
					<br />
					<br />
					&lt;ButtonFor htmlFor="pop-up"&gt;Open PopUp&lt;/ButtonFor&gt;
					<br />
					&lt;PopUp id="pop-up" useCloseBtn=&#123;<strong>false</strong>
					&#125;&gt;...&lt;/PopUp&gt;
				</code>
			</pre>
			<i>
				The following button closes the PopUp but independent of the value of the{' '}
				<code>useCloseBtn</code> attribute, meaning that when this attribute is{' '}
				<code>false</code>, it will not add a button or means to close the PopUp, so a means
				to close the PopUp must be added, if desired.
			</i>
			<br />
			<br />
			<ButtonFor htmlFor="pop-up-use-close-btn">Close PopUp</ButtonFor>
		</PopUp>
	</div>
);

export default ExampleUseCloseInPopUp;
