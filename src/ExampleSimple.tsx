import PopUp from '../lib/PopUp';
import ButtonFor from '@wudev/button-for';

const ExampleCommonUse = () => (
	<div className="example">
		<ButtonFor htmlFor="pop-up-simple-use">Simple Use</ButtonFor>
		<PopUp id="pop-up-simple-use">
			<h3>Simple Use</h3>
			<p>
				Import the component and put the content in the <code>PopUp</code>. It is possible
				to use the{' '}
				<a href="https://www.npmjs.com/package/@wudev/button-for" target="_blank">
					<code>ButtonFor</code>
				</a>{' '}
				component to make interaction with the component much easier.
			</p>
			<pre>
				<code>
					import PopUp from '@wudev/pop-up';
					<br />
					import ButtonFor from '@wudev/button-for';
					<br />
					<br />
					&lt;ButtonFor htmlFor="pop-up"&gt;Open PopUp&lt;/ButtonFor&gt;
					<br />
					&lt;PopUp id="pop-up"&gt;...&lt;/PopUp&gt;
				</code>
			</pre>
		</PopUp>
	</div>
);

export default ExampleCommonUse;
