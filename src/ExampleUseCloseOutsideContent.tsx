import PopUp from '../lib/PopUp';
import ButtonFor from '@wudev/button-for';

const ExampleUseCloseOutsideContent = () => (
	<div className="example">
		<ButtonFor htmlFor="pop-up-close-in-pop-up">Use close outside the content</ButtonFor>
		<PopUp id="pop-up-close-in-pop-up" useCloseOutsideContent={true}>
			<h3>Use close outside the content</h3>
			<p>
				With the <code>useCloseOutsideContent</code> attribute, it is possible to close the
				PopUp by clicking outside the content.
			</p>
			<pre>
				<code>
					import PopUp from '@wudev/pop-up';
					<br />
					import ButtonFor from '@wudev/button-for';
					<br />
					<br />
					&lt;ButtonFor htmlFor="pop-up"&gt;Open PopUp&lt;/ButtonFor&gt;
					<br />
					&lt;PopUp id="pop-up"
					useCloseOutsideContent=&#123;true&#125;&gt;...&lt;/PopUp&gt;
				</code>
			</pre>
		</PopUp>
	</div>
);

export default ExampleUseCloseOutsideContent;
