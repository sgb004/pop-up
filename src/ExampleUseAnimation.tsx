import PopUp from '../lib/PopUp';
import ButtonFor from '@wudev/button-for';

const ExampleUseAnimation = () => (
	<div className="example">
		<ButtonFor htmlFor="pop-up-use-animation">Use Animation</ButtonFor>
		<PopUp id="pop-up-use-animation">
			<h3>Use Animation</h3>
			<p>It is possible to change the animation with the following CSS variables:</p>
			<pre>
				<code>
					#pop-up-id, .pop-up &#123;
					<br />
					{'    '}--animation-open-name: slideInRight;
					<br />
					{'    '}--animation-open-duration: 0.5s;
					<br />
					{'    '}--animation-open-timing-function: linear;
					<br />
					<br />
					{'    '}--animation-close-name: slideOutLeft;
					<br />
					{'    '}--animation-close-duration: 0.5s;
					<br />
					{'    '}--animation-close-timing-function: linear;
					<br />
					<br />
					{'    '}--animation-duration: 0.5s;
					<br />
					{'    '}--animation-timing-function: linear;
					<br />
					&#125;
				</code>
			</pre>
			<p>
				<strong>Where:</strong>
			</p>
			<ul>
				<li>
					<code>--animation-open-name</code> The name of the animation when opening. By
					default <code>slideInRight</code>, this animation is included in the component's
					CSS, so it will not be necessary to replace it.
				</li>
				<li>
					<code>--animation-open-duration</code> The duration of the animation when
					opening. By default <code>0.5s</code>;
				</li>
				<li>
					<code>--animation-open-timing-function</code> The speed curve of the animation
					when opening. By default <code>linear</code>;
					<br />
					<br />
				</li>
				<li>
					<code>--animation-close-name</code> The name of the animation when closing. By
					default <code>slideOutLeft</code>, This animation is included in the component's
					CSS, so it will not be necessary to replace it.
				</li>
				<li>
					<code>--animation-close-duration</code> The duration of the animation when
					closing. By default <code>0.5s</code>;
				</li>
				<li>
					<code>--animation-close-timing-function</code> The speed curve of the animation
					when closing. By default <code>linear</code>;
					<br />
					<br />
				</li>
				<li>
					<code>--animation-duration</code> The duration of the animation when opening and
					closing. By default <code>0.5s</code>;
				</li>
				<li>
					<code>--animation-timing-function</code> The speed curve of the animation when
					opening and closing. By default <code>linear</code>;
				</li>
			</ul>
			<p>
				<strong>Globally, the following variables can be used:</strong>
			</p>
			<pre>
				<code>
					:root, body &#123;
					<br />
					{'    '}--pop-up-animation-open-name: slideInUp;
					<br />
					{'    '}--pop-up-animation-open-duration: 0.25s;
					<br />
					{'    '}--pop-up-animation-open-timing-function: ease-in;
					<br />
					<br />
					{'    '}--pop-up-animation-close-name: slideOutDown;
					<br />
					{'    '}--pop-up-animation-close-duration: 0.25s;
					<br />
					{'    '}--pop-up-animation-close-timing-function: ease-out;
					<br />
					<br />
					{'    '}--pop-animation-duration: 0.25s;
					<br />
					{'    '}--pop-animation-timing-function: ease;
					<br />
					&#125;
				</code>
			</pre>
			<p>
				<strong>Where:</strong>
			</p>
			<ul>
				<li>
					<code>--pop-up-animation-open-name</code> The name of the animation when
					opening.
				</li>
				<li>
					<code>--pop-up-animation-open-duration</code> The duration of the animation when
					opening.
				</li>
				<li>
					<code>--pop-up-animation-open-timing-function</code> The speed curve of the
					animation when opening.
					<br />
					<br />
				</li>
				<li>
					<code>--pop-up-animation-close-name</code> The name of the animation when
					closing.
				</li>
				<li>
					<code>--pop-up-animation-close-duration</code> The duration of the animation
					when closing.
				</li>
				<li>
					<code>--pop-up-animation-close-timing-function</code> The speed curve of the
					animation when closing.
					<br />
					<br />
				</li>
				<li>
					<code>--pop-animation-duration</code> The duration of the animation when opening
					and closing.
				</li>
				<li>
					<code>--pop-animation-timing-function</code> The speed curve of the animation
					when opening and closing.
				</li>
			</ul>
			<p>
				It is possible disable the animation with <code>useAnimation</code> attribute.
			</p>
			<pre>
				<code>
					import PopUp from '@wudev/pop-up';
					<br />
					import ButtonFor from '@wudev/button-for';
					<br />
					<br />
					&lt;ButtonFor htmlFor="pop-up"&gt;Open PopUp&lt;/ButtonFor&gt;
					<br />
					&lt;PopUp id="pop-up" useAnimation=&#123;<strong>false</strong>
					&#125;&gt;...&lt;/PopUp&gt;
				</code>
			</pre>
		</PopUp>
	</div>
);

export default ExampleUseAnimation;
