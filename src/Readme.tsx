import Examples from './Examples';

const Readme = () => (
	<div id="readme">
	<h1>PopUp</h1>
<p><a href="https://www.npmjs.com/package/@wudev/pop-up"><img src="https://img.shields.io/badge/version-1.0.0-blue.svg" alt="Version" /></a>
<a href="https://sgb004.com"><img src="https://img.shields.io/badge/author-sgb004-green.svg" alt="Author" /></a></p>
<p>A PopUp component for React.</p>
<h2>Installation</h2>
<p>To install <strong>ButtonFor</strong> you can use npm:</p>
<pre><code>npm install @wudev/pop-up<br/></code></pre>
<Examples />

<h2>Usage</h2>
<h3>Simple Use</h3>
<p>Import the component and put the content in the <code>PopUp</code>. It is possible to use the <a href="https://www.npmjs.com/package/@wudev/button-for"><code>ButtonFor</code></a> component to make interaction with the component much easier.</p>
{/* prettier-ignore-start */}
<pre><code>import PopUp from '@wudev/pop-up';<br/>import ButtonFor from '@wudev/button-for';<br/><br/>&lt;ButtonFor htmlFor="pop-up"&gt;Open PopUp&lt;/ButtonFor&gt;<br/>&lt;PopUp id="pop-up"&gt;...&lt;/PopUp&gt;<br/></code></pre>
{/* prettier-ignore-end */}
<h3>Use close button</h3>
<p>By default the close button is enabled, with the <code>useCloseBtn</code> attribute, it is possible disable the close button.</p>
{/* prettier-ignore-start */}
<pre><code>import PopUp from '@wudev/pop-up';<br/>import ButtonFor from '@wudev/button-for';<br/><br/>&lt;ButtonFor htmlFor="pop-up"&gt;Open PopUp&lt;/ButtonFor&gt;<br/>&lt;PopUp id="pop-up" useCloseBtn=&#123;false&#125;&gt;...&lt;/PopUp&gt;<br/></code></pre>
{/* prettier-ignore-end */}
<h3>Use close in PopUp</h3>
<p>With the <code>useCloseInPopUp</code> attribute, it is possible to close the PopUp by clicking outside the content.</p>
{/* prettier-ignore-start */}
<pre><code>import PopUp from '@wudev/pop-up';<br/>import ButtonFor from '@wudev/button-for';<br/><br/>&lt;ButtonFor htmlFor="pop-up"&gt;Open PopUp&lt;/ButtonFor&gt;<br/>&lt;PopUp id="pop-up" useCloseInPopUp=&#123;true&#125;&gt;...&lt;/PopUp&gt;<br/></code></pre>
{/* prettier-ignore-end */}
<h3>Use close outside the content</h3>
<p>With the <code>useCloseOutsideContent</code> attribute, it is possible to close the PopUp by clicking outside the content.</p>
{/* prettier-ignore-start */}
<pre><code>import PopUp from '@wudev/pop-up';<br/>import ButtonFor from '@wudev/button-for';<br/><br/>&lt;ButtonFor htmlFor="pop-up"&gt;Open PopUp&lt;/ButtonFor&gt;<br/>&lt;PopUp id="pop-up" useCloseOutsideContent=&#123;true&#125;&gt;...&lt;/PopUp&gt;<br/></code></pre>
{/* prettier-ignore-end */}
<h3>Use close button in content</h3>
<p>It is possible to put the close button into the content with the class <code>use-close-btn-in-content</code>.</p>
{/* prettier-ignore-start */}
<pre><code>import PopUp from '@wudev/pop-up';<br/>import ButtonFor from '@wudev/button-for';<br/><br/>&lt;ButtonFor htmlFor="pop-up"&gt;Open PopUp&lt;/ButtonFor&gt;<br/>&lt;PopUp id="pop-up" className="use-close-btn-in-content"&gt;...&lt;/PopUp&gt;<br/></code></pre>
{/* prettier-ignore-end */}
<h3>Use Animation</h3>
<p>It is possible to change the animation with the following CSS variables:</p>
{/* prettier-ignore-start */}
<pre><code>#pop-up-id, .pop-up &#123;<br/>    --animation-open-name: slideInRight;<br/>    --animation-open-duration: 0.5s;<br/>    --animation-open-timing-function: linear;<br/><br/>    --animation-close-name: slideOutLeft;<br/>    --animation-close-duration: 0.5s;<br/>    --animation-close-timing-function: linear;<br/><br/>    --animation-duration: 0.5s;<br/>    --animation-timing-function: linear;<br/>&#125;<br/></code></pre>
{/* prettier-ignore-end */}
<p><strong>Where:</strong></p>
<ul>
<li><code>--animation-open-name</code> The name of the animation when opening. By default <code>slideInRight</code>, this animation is included in the component's CSS, so it will not be necessary to replace it.</li>
<li><code>--animation-open-duration</code> The duration of the animation when opening. By default <code>0.5s</code>;</li>
<li><code>--animation-open-timing-function</code> The speed curve of the animation when opening. By default <code>linear</code>;</li>
<li><code>--animation-close-name</code> The name of the animation when closing. By default <code>slideOutLeft</code>, This animation is included in the component's CSS, so it will not be necessary to replace it.</li>
<li><code>--animation-close-duration</code> The duration of the animation when closing. By default <code>0.5s</code>;</li>
<li><code>--animation-close-timing-function</code> The speed curve of the animation when closing. By default <code>linear</code>;</li>
<li><code>--animation-duration</code> The duration of the animation when opening and closing. By default <code>0.5s</code>;</li>
<li><code>--animation-timing-function</code> The speed curve of the animation when opening and closing. By default <code>linear</code>;</li>
</ul>
<p><strong>Globally, the following variables can be used:</strong></p>
{/* prettier-ignore-start */}
<pre><code>:root, body &#123;<br/>    --pop-up-animation-open-name: slideInUp;<br/>    --pop-up-animation-open-duration: 0.25s;<br/>    --pop-up-animation-open-timing-function: ease-in;<br/><br/>    --pop-up-animation-close-name: slideOutDown;<br/>    --pop-up-animation-close-duration: 0.25s;<br/>    --pop-up-animation-close-timing-function: ease-out;<br/><br/>    --pop-animation-duration: 0.25s;<br/>    --pop-animation-timing-function: ease;<br/>&#125;<br/></code></pre>
{/* prettier-ignore-end */}
<p><strong>Where:</strong></p>
<ul>
<li><code>--pop-up-animation-open-name</code> The name of the animation when opening.</li>
<li><code>--pop-up-animation-open-duration</code> The duration of the animation when opening.</li>
<li><code>--pop-up-animation-open-timing-function</code> The speed curve of the animation when opening.</li>
<li><code>--pop-up-animation-close-name</code> The name of the animation when closing.</li>
<li><code>--pop-up-animation-close-duration</code> The duration of the animation when closing.</li>
<li><code>--pop-up-animation-close-timing-function</code> The speed curve of the animation when closing.</li>
<li><code>--pop-animation-duration</code> The duration of the animation when opening and closing.</li>
<li><code>--pop-animation-timing-function</code> The speed curve of the animation when opening and closing.</li>
</ul>
<p>It is possible disable the animation with <code>useAnimation</code> attribute.</p>
{/* prettier-ignore-start */}
<pre><code>import PopUp from '@wudev/pop-up';<br/>import ButtonFor from '@wudev/button-for';<br/><br/>&lt;ButtonFor htmlFor="pop-up"&gt;Open PopUp&lt;/ButtonFor&gt;<br/>&lt;PopUp id="pop-up" useAnimation=&#123;false&#125;&gt;...&lt;/PopUp&gt;<br/></code></pre>
{/* prettier-ignore-end */}
<h3>Use Animation in content</h3>
<p>It is possible to animate the content with <code>useAnimationInContent</code> attribute.</p>
{/* prettier-ignore-start */}
<pre><code>import PopUp from '@wudev/pop-up';<br/>import ButtonFor from '@wudev/button-for';<br/><br/>&lt;ButtonFor htmlFor="pop-up"&gt;Open PopUp&lt;/ButtonFor&gt;<br/>&lt;PopUp id="pop-up" useAnimationInContent=&#123;true&#125;&gt;...&lt;/PopUp&gt;<br/></code></pre>
{/* prettier-ignore-end */}
<p>Once the <code>useAnimationInContent</code> attribute is activated, it will be necessary to define the open and close animations.<br />
It is possible to use the following configurations to define the animations, as well as change their duration and speed curve:</p>
{/* prettier-ignore-start */}
<pre><code>#pop-up-id, .pop-up &#123;<br/>    --animation-content-open-name: fadeIn;<br/>    --animation-content-open-duration: 0.5s;<br/>    --animation-content-open-timing-function: linear;<br/><br/>    --animation-content-close-name: fadeOut;<br/>    --animation-content-close-duration: 0.5s;<br/>    --animation-content-close-timing-function: linear;<br/>&#125;<br/></code></pre>
{/* prettier-ignore-end */}
<p><strong>Where:</strong></p>
<ul>
<li><code>--animation-content-open-name</code> The name of the animation when opening.</li>
<li><code>--animation-content-open-duration</code> The duration of the animation when opening. By default the value defined in the <code>--animation-duration</code> variable;</li>
<li><code>--animation-content-open-timing-function</code> The speed curve of the animation when opening. By default the value defined in the <code>--animation-timing-function</code> variable;</li>
<li><code>--animation-content-close-name</code> The name of the animation when closing.</li>
<li><code>--animation-content-close-duration</code> The duration of the animation when closing. By default the value defined in the <code>--animation-duration</code> variable;</li>
<li><code>--animation-content-close-timing-function</code> The speed curve of the animation when closing. By default the value defined in the <code>--animation-timing-function</code> variable;</li>
</ul>
<p><strong>Globally, the following variables can be used:</strong></p>
{/* prettier-ignore-start */}
<pre><code>:root, body &#123;<br/>    --pop-up-animation-content-open-name: fadeIn;<br/>    --pop-up-animation-content-open-duration: 0.25s;<br/>    --pop-up-animation-content-open-timing-function: ease-in;<br/><br/>    --pop-up-animation-content-close-name: fadeOut;<br/>    --pop-up-animation-content-close-duration: 0.25s;<br/>    --pop-up-animation-content-close-timing-function: ease-out;<br/>&#125;<br/></code></pre>
{/* prettier-ignore-end */}
<p><strong>Where:</strong></p>
<ul>
<li><code>--pop-up-animation-content-open-name</code> The name of the animation when opening.</li>
<li><code>--pop-up-animation-content-open-duration</code> The duration of the animation when opening.</li>
<li><code>--pop-up-animation-content-open-timing-function</code> The speed curve of the animation when opening.</li>
<li><code>--pop-up-animation-content-close-name</code> The name of the animation when closing.</li>
<li><code>--pop-up-animation-content-close-duration</code> The duration of the animation when closing.</li>
<li><code>--pop-up-animation-content-close-timing-function</code> The speed curve of the animation when closing.</li>
</ul>
<h2>Author</h2>
<p><a href="https://sgb004.com">sgb004</a></p>
<h2>License</h2>
<p><a href="LICENSE">MIT</a></p>
	</div>
);

export default Readme;
	