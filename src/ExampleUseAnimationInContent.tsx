import PopUp from '../lib/PopUp';
import ButtonFor from '@wudev/button-for';

const ExampleUseAnimationInContent = () => (
	<div className="example">
		<ButtonFor htmlFor="pop-up-use-animation-in-content">Use Animation in content</ButtonFor>
		<PopUp id="pop-up-use-animation-in-content" useAnimationInContent={true}>
			<h3>Use Animation in content</h3>
			<p>
				It is possible to animate the content with <code>useAnimationInContent</code>{' '}
				attribute.
			</p>
			<pre>
				<code>
					import PopUp from '@wudev/pop-up';
					<br />
					import ButtonFor from '@wudev/button-for';
					<br />
					<br />
					&lt;ButtonFor htmlFor="pop-up"&gt;Open PopUp&lt;/ButtonFor&gt;
					<br />
					&lt;PopUp id="pop-up" useAnimationInContent=&#123;<strong>true</strong>
					&#125;&gt;...&lt;/PopUp&gt;
				</code>
			</pre>
			<p>
				Once the <code>useAnimationInContent</code> attribute is activated, it will be
				necessary to define the open and close animations.
				<br />
				It is possible to use the following configurations to define the animations, as well
				as change their duration and speed curve:
			</p>
			<pre>
				<code>
					#pop-up-id, .pop-up &#123;
					<br />
					{'    '}--animation-content-open-name: fadeIn;
					<br />
					{'    '}--animation-content-open-duration: 0.5s;
					<br />
					{'    '}--animation-content-open-timing-function: linear;
					<br />
					<br />
					{'    '}--animation-content-close-name: fadeOut;
					<br />
					{'    '}--animation-content-close-duration: 0.5s;
					<br />
					{'    '}--animation-content-close-timing-function: linear;
					<br />
					&#125;
				</code>
			</pre>
			<p>
				<strong>Where:</strong>
			</p>
			<ul>
				<li>
					<code>--animation-content-open-name</code> The name of the animation when
					opening.
				</li>
				<li>
					<code>--animation-content-open-duration</code> The duration of the animation
					when opening. By default the value defined in the{' '}
					<code>--animation-duration</code> variable;
				</li>
				<li>
					<code>--animation-content-open-timing-function</code> The speed curve of the
					animation when opening. By default the value defined in the{' '}
					<code>--animation-timing-function</code> variable;
					<br />
					<br />
				</li>
				<li>
					<code>--animation-content-close-name</code> The name of the animation when
					closing.
				</li>
				<li>
					<code>--animation-content-close-duration</code> The duration of the animation
					when closing. By default the value defined in the{' '}
					<code>--animation-duration</code> variable;
				</li>
				<li>
					<code>--animation-content-close-timing-function</code> The speed curve of the
					animation when closing. By default the value defined in the{' '}
					<code>--animation-timing-function</code> variable;
					<br />
					<br />
				</li>
			</ul>
			<p>
				<strong>Globally, the following variables can be used:</strong>
			</p>
			<pre>
				<code>
					:root, body &#123;
					<br />
					{'    '}--pop-up-animation-content-open-name: fadeIn;
					<br />
					{'    '}--pop-up-animation-content-open-duration: 0.25s;
					<br />
					{'    '}--pop-up-animation-content-open-timing-function: ease-in;
					<br />
					<br />
					{'    '}--pop-up-animation-content-close-name: fadeOut;
					<br />
					{'    '}--pop-up-animation-content-close-duration: 0.25s;
					<br />
					{'    '}--pop-up-animation-content-close-timing-function: ease-out;
					<br />
					&#125;
				</code>
			</pre>
			<p>
				<strong>Where:</strong>
			</p>
			<ul>
				<li>
					<code>--pop-up-animation-content-open-name</code> The name of the animation when
					opening.
				</li>
				<li>
					<code>--pop-up-animation-content-open-duration</code> The duration of the
					animation when opening.
				</li>
				<li>
					<code>--pop-up-animation-content-open-timing-function</code> The speed curve of
					the animation when opening.
					<br />
					<br />
				</li>
				<li>
					<code>--pop-up-animation-content-close-name</code> The name of the animation
					when closing.
				</li>
				<li>
					<code>--pop-up-animation-content-close-duration</code> The duration of the
					animation when closing.
				</li>
				<li>
					<code>--pop-up-animation-content-close-timing-function</code> The speed curve of
					the animation when closing.
					<br />
					<br />
				</li>
			</ul>
		</PopUp>
	</div>
);

export default ExampleUseAnimationInContent;
