# PopUp

[![Version](https://img.shields.io/badge/version-1.0.0-blue.svg)](https://www.npmjs.com/package/@wudev/pop-up)
[![Author](https://img.shields.io/badge/author-sgb004-green.svg)](https://sgb004.com)

A PopUp component for React.

## Installation

To install **ButtonFor** you can use npm:

```
npm install @wudev/pop-up
```

<!-- examples -->

<!-- start-remove-in-html -->

[Check online](https://pop-up.sgb004.com/)

<!-- end-remove-in-html -->

## Usage

### Simple Use

Import the component and put the content in the `PopUp`. It is possible to use the [`ButtonFor`](https://www.npmjs.com/package/@wudev/button-for) component to make interaction with the component much easier.

<!-- prettier-ignore-start -->
```jsx
import PopUp from '@wudev/pop-up';
import ButtonFor from '@wudev/button-for';

<ButtonFor htmlFor="pop-up">Open PopUp</ButtonFor>
<PopUp id="pop-up">...</PopUp>
```
<!-- prettier-ignore-end -->

### Use close button

By default the close button is enabled, with the `useCloseBtn` attribute, it is possible disable the close button.

<!-- prettier-ignore-start -->
```jsx
import PopUp from '@wudev/pop-up';
import ButtonFor from '@wudev/button-for';

<ButtonFor htmlFor="pop-up">Open PopUp</ButtonFor>
<PopUp id="pop-up" useCloseBtn={false}>...</PopUp>
```
<!-- prettier-ignore-end -->

### Use close in PopUp

With the `useCloseInPopUp` attribute, it is possible to close the PopUp by clicking outside the content.

<!-- prettier-ignore-start -->
```jsx
import PopUp from '@wudev/pop-up';
import ButtonFor from '@wudev/button-for';

<ButtonFor htmlFor="pop-up">Open PopUp</ButtonFor>
<PopUp id="pop-up" useCloseInPopUp={true}>...</PopUp>
```
<!-- prettier-ignore-end -->

### Use close outside the content

With the `useCloseOutsideContent` attribute, it is possible to close the PopUp by clicking outside the content.

<!-- prettier-ignore-start -->
```jsx
import PopUp from '@wudev/pop-up';
import ButtonFor from '@wudev/button-for';

<ButtonFor htmlFor="pop-up">Open PopUp</ButtonFor>
<PopUp id="pop-up" useCloseOutsideContent={true}>...</PopUp>
```
<!-- prettier-ignore-end -->

### Use close button in content

It is possible to put the close button into the content with the class `use-close-btn-in-content`.

<!-- prettier-ignore-start -->
```jsx
import PopUp from '@wudev/pop-up';
import ButtonFor from '@wudev/button-for';

<ButtonFor htmlFor="pop-up">Open PopUp</ButtonFor>
<PopUp id="pop-up" className="use-close-btn-in-content">...</PopUp>
```
<!-- prettier-ignore-end -->

### Use Animation

It is possible to change the animation with the following CSS variables:

<!-- prettier-ignore-start -->
```css
#pop-up-id, .pop-up {
    --animation-open-name: slideInRight;
    --animation-open-duration: 0.5s;
    --animation-open-timing-function: linear;

    --animation-close-name: slideOutLeft;
    --animation-close-duration: 0.5s;
    --animation-close-timing-function: linear;

    --animation-duration: 0.5s;
    --animation-timing-function: linear;
}
```
<!-- prettier-ignore-end -->

**Where:**

-   `--animation-open-name` The name of the animation when opening. By default `slideInRight`, this animation is included in the component's CSS, so it will not be necessary to replace it.
-   `--animation-open-duration` The duration of the animation when opening. By default `0.5s`;
-   `--animation-open-timing-function` The speed curve of the animation when opening. By default `linear`;
-   `--animation-close-name` The name of the animation when closing. By default `slideOutLeft`, This animation is included in the component's CSS, so it will not be necessary to replace it.
-   `--animation-close-duration` The duration of the animation when closing. By default `0.5s`;
-   `--animation-close-timing-function` The speed curve of the animation when closing. By default `linear`;
-   `--animation-duration` The duration of the animation when opening and closing. By default `0.5s`;
-   `--animation-timing-function` The speed curve of the animation when opening and closing. By default `linear`;

**Globally, the following variables can be used:**

<!-- prettier-ignore-start -->
```css
:root, body {
	--pop-up-animation-open-name: slideInUp;
	--pop-up-animation-open-duration: 0.25s;
	--pop-up-animation-open-timing-function: ease-in;

	--pop-up-animation-close-name: slideOutDown;
	--pop-up-animation-close-duration: 0.25s;
	--pop-up-animation-close-timing-function: ease-out;

	--pop-animation-duration: 0.25s;
	--pop-animation-timing-function: ease;
}
```
<!-- prettier-ignore-end -->

**Where:**

-   `--pop-up-animation-open-name` The name of the animation when opening.
-   `--pop-up-animation-open-duration` The duration of the animation when opening.
-   `--pop-up-animation-open-timing-function` The speed curve of the animation when opening.
-   `--pop-up-animation-close-name` The name of the animation when closing.
-   `--pop-up-animation-close-duration` The duration of the animation when closing.
-   `--pop-up-animation-close-timing-function` The speed curve of the animation when closing.
-   `--pop-animation-duration` The duration of the animation when opening and closing.
-   `--pop-animation-timing-function` The speed curve of the animation when opening and closing.

It is possible disable the animation with `useAnimation` attribute.

<!-- prettier-ignore-start -->
```jsx
import PopUp from '@wudev/pop-up';
import ButtonFor from '@wudev/button-for';

<ButtonFor htmlFor="pop-up">Open PopUp</ButtonFor>
<PopUp id="pop-up" useAnimation={false}>...</PopUp>
```
<!-- prettier-ignore-end -->

### Use Animation in content

It is possible to animate the content with `useAnimationInContent` attribute.

<!-- prettier-ignore-start -->
```jsx
import PopUp from '@wudev/pop-up';
import ButtonFor from '@wudev/button-for';

<ButtonFor htmlFor="pop-up">Open PopUp</ButtonFor>
<PopUp id="pop-up" useAnimationInContent={true}>...</PopUp>
```
<!-- prettier-ignore-end -->

Once the `useAnimationInContent` attribute is activated, it will be necessary to define the open and close animations.  
It is possible to use the following configurations to define the animations, as well as change their duration and speed curve:

<!-- prettier-ignore-start -->
```css
#pop-up-id, .pop-up {
    --animation-content-open-name: fadeIn;
    --animation-content-open-duration: 0.5s;
    --animation-content-open-timing-function: linear;

    --animation-content-close-name: fadeOut;
    --animation-content-close-duration: 0.5s;
    --animation-content-close-timing-function: linear;
}
```
<!-- prettier-ignore-end -->

**Where:**

-   `--animation-content-open-name` The name of the animation when opening.
-   `--animation-content-open-duration` The duration of the animation when opening. By default the value defined in the `--animation-duration` variable;
-   `--animation-content-open-timing-function` The speed curve of the animation when opening. By default the value defined in the `--animation-timing-function` variable;
-   `--animation-content-close-name` The name of the animation when closing.
-   `--animation-content-close-duration` The duration of the animation when closing. By default the value defined in the `--animation-duration` variable;
-   `--animation-content-close-timing-function` The speed curve of the animation when closing. By default the value defined in the `--animation-timing-function` variable;

**Globally, the following variables can be used:**

<!-- prettier-ignore-start -->
```css
:root, body {
    --pop-up-animation-content-open-name: fadeIn;
    --pop-up-animation-content-open-duration: 0.25s;
    --pop-up-animation-content-open-timing-function: ease-in;

    --pop-up-animation-content-close-name: fadeOut;
    --pop-up-animation-content-close-duration: 0.25s;
    --pop-up-animation-content-close-timing-function: ease-out;
}
```
<!-- prettier-ignore-end -->

**Where:**

-   `--pop-up-animation-content-open-name` The name of the animation when opening.
-   `--pop-up-animation-content-open-duration` The duration of the animation when opening.
-   `--pop-up-animation-content-open-timing-function` The speed curve of the animation when opening.
-   `--pop-up-animation-content-close-name` The name of the animation when closing.
-   `--pop-up-animation-content-close-duration` The duration of the animation when closing.
-   `--pop-up-animation-content-close-timing-function` The speed curve of the animation when closing.

## Author

[sgb004](https://sgb004.com)

## License

[MIT](LICENSE)
