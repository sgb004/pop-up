import { LegacyRef, RefObject, forwardRef, useEffect, useRef } from 'react';
import CloseBtn from '../CloseBtn';
import close from './close';
import './styles.css';
import 'animate.css/source/sliding_entrances/slideInRight.css';
import 'animate.css/source/sliding_exits/slideOutLeft.css';

export type PopUpProps = React.ComponentProps<'dialog'> & {
	useCloseBtn?: boolean;
	useCloseOutsideContent?: boolean;
	useAnimation?: boolean;
	useAnimationInContent?: boolean;
};

const PopUp = forwardRef<HTMLDialogElement, PopUpProps>((props, ref) => {
	const popUpRef = useRef(ref);
	const {
		['useCloseBtn']: useCloseBtn,
		['useCloseOutsideContent']: useCloseOutsideContent,
		['useAnimation']: useAnimation,
		['useAnimationInContent']: useAnimationInContent,
		...p
	} = props;

	useEffect(() => {
		const _popUpRef = (ref ??
			(popUpRef as LegacyRef<HTMLDialogElement>)) as RefObject<HTMLDialogElement>;
		let removeListener = () => {};

		if (useAnimation !== false && _popUpRef && _popUpRef.current instanceof HTMLDialogElement) {
			const popUp = _popUpRef.current;
			const onAfterClose = () => {
				popUp.open = true;
				popUp.classList.add('close');
			};

			popUp.addEventListener('afterClose', onAfterClose);
			removeListener = () => popUp.removeEventListener('afterClose', onAfterClose);
		}

		return () => removeListener();
	}, [ref, useAnimation]);

	return (
		<dialog
			{...p}
			ref={ref ?? (popUpRef as LegacyRef<HTMLDialogElement>)}
			className={`pop-up ${useCloseOutsideContent ? 'use-close-in-pop-up' : ''} ${
				useAnimation === false ? 'no-use-animation' : ''
			} ${useAnimationInContent ? 'use-animation-in-content content-display-none' : ''} ${
				props.className ?? ''
			}`}
			onClick={(event) => {
				if (useCloseOutsideContent && event.target instanceof HTMLDialogElement) {
					close(event.target);
				}
				if (props.onClose) {
					props.onClose(event);
				}
			}}
			onAnimationEnd={(event) => {
				if (useAnimation !== false) {
					const popUp = event.target;
					if (popUp instanceof HTMLDialogElement && popUp.classList.contains('close')) {
						popUp.classList.remove('close');
						popUp.open = false;

						if (useAnimationInContent) {
							popUp.classList.add('content-display-none');
						}
					} else if (useAnimationInContent && popUp instanceof HTMLDialogElement) {
						popUp.classList.remove('content-display-none');
					}
				}
			}}
		>
			<div className="pop-up-content">
				{props.children}
				{useCloseBtn !== false && (
					<CloseBtn
						popUpRef={ref ?? (popUpRef as LegacyRef<HTMLDialogElement>)}
						className="pop-up-close-btn"
					></CloseBtn>
				)}
			</div>
		</dialog>
	);
});

export default PopUp;
