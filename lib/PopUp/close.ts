export const close = (popUp: HTMLDialogElement) => {
	const beforeCloseEvent = new Event('beforeClose', {
		bubbles: true,
		cancelable: true,
	});
	const beforeCloseResult = popUp.dispatchEvent(beforeCloseEvent);

	if (beforeCloseResult) {
		const afterCloseEvent = new Event('afterClose', {
			bubbles: true,
			cancelable: true,
		});

		popUp.open = false;

		popUp.dispatchEvent(afterCloseEvent);
	}
};

export default close;
