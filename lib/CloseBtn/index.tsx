import { LegacyRef } from 'react';
import close from '../PopUp/close';

export type CloseBtnProps = React.ComponentProps<'button'> & {
	popUpRef: LegacyRef<HTMLDialogElement>;
};

const CloseBtn = (props: CloseBtnProps) => {
	const { ['popUpRef']: popUpRef, ...p } = props;

	return (
		<button
			{...p}
			onClick={() => {
				const popUp = popUpRef as React.MutableRefObject<HTMLDialogElement> | null;
				if (popUp && popUp.current instanceof HTMLDialogElement) {
					close(popUp.current);
				}
			}}
		>
			{props.children}
		</button>
	);
};

export default CloseBtn;
